/*
 * exp9_brushless_motor.c
 *
 * Created: 2018/11/7 下午 04:58:20
 * Author : zxpay
 */ 
#define F_CPU 1105922
#include <avr/io.h>
#include "ASA_Lib.h"
#include "M128_Danny_Timer.h"

volatile char flag = 1;
ISR(TIMER0_COMP_vect){
	if(flag == 1){
		PORTA = 0b00100101;
		flag = 2;
	}
	else if(flag == 2){
		PORTA = 0b01100001;
		flag = 3;
	}
	else if(flag == 3){
		PORTA = 0b01000011;
		flag = 4;
	}
	else if(flag == 4){
		PORTA = 0b01010010;
		flag = 5;
	}
	else if(flag == 5){
		PORTA = 0b00010110;
		flag = 6;
	}
	else if(flag == 6){
		PORTA = 0b00110100;
		flag = 1;
	}
}


int main(void){
    ASA_M128_set();
	printf("Run main code ~~~\n");
	DDRA = 0xff;
	setTimer0_CompareInterrupt(255, 1024);  
	openTimer0();
	for(int i=255;i>=20;i--){
		OCR0 = i;
		_delay_ms(100);
	}
/*
	setTimer0_CompareInterrupt(120, 512);  
		for(int i=120;i>=50;i--){
			OCR0 = i;
			_delay_ms(1000);
		}*/

	while (1) {

    }
	return 0;
}
